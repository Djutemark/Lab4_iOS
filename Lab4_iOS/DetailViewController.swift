//
//  DetailViewController.swift
//  Lab4_iOS
//
//  Created by Dennis Jutemark on 2017-11-24.
//  Copyright © 2017 Dennis Jutemark. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var navTitle: UINavigationItem!
    
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var manufracturerLabel: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var crewLabel: UILabel!
    @IBOutlet weak var passengersLabel: UILabel!
    
    public var starship: Starship?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle.title = starship?.name
        modelLabel.text = starship?.model!
        manufracturerLabel.text = starship?.manufacturer!
        costLabel.text = (starship?.cost_in_credits!)! + " credits"
        crewLabel.text = starship?.crew!
        passengersLabel.text = starship?.passengers!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
