//
//  ViewController.swift
//  Lab4_iOS
//
//  Created by Dennis Jutemark on 2017-11-24.
//  Copyright © 2017 Dennis Jutemark. All rights reserved.
//

import UIKit

class StarshipRow: UITableViewCell {
    @IBOutlet weak var StarshipName: UILabel!
}

class Starship: Codable {
    var name, model, manufacturer, cost_in_credits, crew, passengers: String?
}

class StarshipRaw: Codable {
    var count: Int?
    var results: [Starship]?
}

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        getStarships()
    }
    
    var starships = StarshipRaw()

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "toDetail") {
            let controller = segue.destination as! DetailViewController
            let name = (sender as? StarshipRow)?.StarshipName.text
            
            for starship in self.starships.results! {
                if starship.name == name {
                    controller.starship = starship
                    break
                }
            }
        }
    }
    
    func getStarships() -> Void {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "swapi.co"
        urlComponents.path = "/api/starships/"
        
        var request = URLRequest(url: urlComponents.url!)
        request.httpMethod = "GET"
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        
        session.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                guard error == nil else {
                    print("error was not null: ", error as Any)
                    return
                }
                
                guard let json = data else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Data was not retrieved from request"]) as Error
                    print(error)
                    return
                }
                let decoder = JSONDecoder()
                
                do {
                    self.starships = try decoder.decode(StarshipRaw.self, from: json)
                    self.tableView.beginUpdates()
                    var counter = 0
                    for _ in self.starships.results! {
                        self.tableView.insertRows(at: [IndexPath(row: counter, section: 0)], with: .automatic)
                        counter += 1
                    }
                    self.tableView.endUpdates()
                    
                } catch {
                    print("There was en error :(")
                }
            }
        }.resume()
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return starships.results?.count ?? 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "starshipRow") as! StarshipRow
        cell.StarshipName.text = starships.results![indexPath.row].name
        return cell
    }
}

